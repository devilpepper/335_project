;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;MapReduce definition
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;for now, map2 will just be map.
;Will try to make concurrent map later
(define map2 map)

;takes the left most item and applies the reduce function to it
;and the curent result
(define reduce-left
  (lambda (f value lst)
        (if (null? lst) value
            (reduce-left f (f value (car lst)) (cdr lst)))))
;mapreduce is done simply by mapping a set of data to some
;tuples produced by a mapping function, followed by a reduction
;function that simplifies the tuples. Reduce-left applies the
;reduction to the tuple list
(define mapreduce
  (lambda (mapper reducer base-case data)
    (reduce-left reducer base-case (map2 mapper data))))
;We will refer to a function defined by map reduce as
;funct = mapreduce(mapper, reducer)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Sample data to try
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define loremipsum
  '((www.lipsum.com em ipsum dolor Lorem Lorem sit amet has omnis malis)
    (www.pericula.com causae eu ne dolorem consequat qui. Ut pericula)
    (www.colupat.com Lorem volutpat ius ei In duo soleat fastidii ne)
    (www.loremipsum.com Lorem ipsum dolor sit amet has Lorem omnis)
    (www.dolorem.com dolorem consequat qui. Ut pericula interpretaris)
    (www.soleat.com  ius ei In duo soleat fastidii ne oratio alterum)
	))

;all contains the
(define theList 
  '((www.boxers.com the five boxing wizards jump quickly)
    (www.foxy.com the quick brown fox jumps over the lazy dog)
    (www.botfather.com provide a list of commands to the BotFather)
    (www.monkeys.com no more monkey jumping on the bed)
    (www.theone.com the Prime Program if the one)
	))
(define theWord 'the)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Part I. simple search using word count
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Summation function
(define (sumLst lst)
  (cond ((null? lst) 0)
        ((pair? lst) (+ (car lst) (sumLst (cdr lst))))
        (else lst)))

;should return (url found found found found ...)
;found is a numerical replacement for each word
;If found, it's 1, otherwise it's 0
(define gMap
  (lambda (value)
	(define (gMapper data)
	  (define (gMapped lst)
	  	(cond ((null? lst) '())
                      ((pair? lst) 
			   (cons (if (eq? (car lst) value) 1 0) (gMapped (cdr lst))))
			  (else (if (= lst value) 1 0))
	   ))
		(cons (car data) (gMapped (cdr data)))
			)
	  (lambda (d) (gMapper d))
	  )
  )

;Returns a list of urls from lists whose sum of the cdr is greater than 0
;(at least 1 of the word was found)
(define gReduce
  (lambda (results lst)
	;expects lst to be a valid value from gMap
	(if (> (sumLst (cdr lst)) 0) (cons (car lst) results) results)
	 ))

;We define google = mapReduce(gMap, gReduce)
(define google
  (lambda (word pages)
    (mapreduce (gMap word) gReduce '() pages)))

;;;;;;;;;;
;;try it;;
;;;;;;;;;;
(google theWord theList)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Part 2. Search using tf-ldf
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;For all 3 parts of tf-ldf, reduce is just a sum
(define reducer
  (lambda (results lst)
	(cons (sumLst lst) results)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Define wordCount using mapReduce;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Just returns a list of 1s for each word that was found
(define wcMap
  (lambda (word)
	(define (wcMapper doc)
	  	(cond ((null? doc) '())
                      ((pair? doc) (if (eq? (car doc) word) (cons 1 (wcMapper (cdr doc)))
                                       (wcMapper (cdr doc))))
                      (else (if (eq? doc word) 1 0))))
	  (lambda (d) (wcMapper d))))

;definition using mapreduce :D
(define wordCount
  (lambda (word pages)
    (mapreduce (wcMap word) reducer '() pages)))

;;;;;;;;;;
;;try it;;
;;;;;;;;;;
(wordCount theWord theList)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Define wordsPerDoc using mapReduce;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;filter undefined...
(define filter
    (lambda (pred lst)
      (cond ((null? lst) '())
            ((pred (car lst)) (cons (car lst) (filter pred (cdr lst))))
            (else (filter pred (cdr lst))))))

;use cdr of doc
;The filter prevents doing word count for a word twice
(define wpdMap
 (lambda (doc)
	(cond ((null? doc) '())
              ;(else (cons (cons (car doc) (wordCount (car doc) doc))
              ((pair? doc) (cons (car (wordCount (car doc) (cons doc '())))
                          (wpdmap (filter (lambda (e) (not (equal? e (car doc)))) (cdr doc)))))
              (else 1))) ;If the last word hasn't been counted, it's count is 1
  )
;definition using mapreduce :D
(define wordsPerDoc
  (lambda (pages)
    (mapreduce wpdMap reducer '() pages)))

;;;;;;;;;;
;;try it;;
;;;;;;;;;;
(wordsPerDoc theList)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Define docsPerWord using mapReduce;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Just returns a list of 1s for each word that was found
(define dpwMap
  (lambda (docs)
	(define (dpwMapper word)
	  (define (findInDoc d)
		  (cond ((null? d) 0)
				((eq? (car d) word) 1)
				(else (findInDoc (cdr d)))))
	  	(map findInDoc docs))
	  (lambda (w) (dpwMapper w))))

;definition using mapreduce :D
(define docsPerWord
  (lambda (word pages)
    (mapreduce (dpwMap pages) reducer '() (cons word '()))))

;;;;;;;;;;
;;try it;;
;;;;;;;;;;
(docsPerWord theWord theList)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Define tf-ldf using mapReduce and above functions;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;tfidfMap(dpw word) => tfidfMapper(doc) => (word wordCount wordsPerDoc docsPerWord)
(define tfidfMap
  (lambda (dpw word)
	(define (tfidfMapper doc)
	  (list (car doc) (car (wordCount word (cons doc '()))) (car (wordsPerDoc (cons doc '()))) dpw))
	  (lambda (d) (tfidfMapper d))))

(define (tfidfCalc occLenDocs)
  (* (/ (car occLenDocs) (cadr occLenDocs)) (log (caddr occLenDocs))))

(define (tiReduce tl)
  (list (car tl) (tfidfCalc (cdr tl))))

(define tfidfReduce
  (lambda (results tl)
	(cons (tiReduce tl) results)))

(define (len lst)
  (cond ((null? lst) 0)
		((pair? lst) (+ 1 (len (cdr lst))))
		(else 1)))

;definition using mapreduce :D
(define tf-idf
  (lambda (word pages)
    (mapreduce (tfidfMap (/ (len pages) (car (docsPerWord word pages))) word) tfidfReduce '() pages)))

;;;;;;;;;;
;;try it;;
;;;;;;;;;;
(tf-idf theWord theList)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Define ranking using mapReduce and tf-ldf;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (rankMap word docs)
  (define (getDoc d lst)
	(cond ((null? d) '())
		  ((eq? (car d) (caar lst)) (car lst))
		  (else (getDoc d (cdr lst)))))
  (lambda (doc) (getDoc doc (tf-idf word docs))))

;rankReduce sorts by weight and eliminates 0 weights
(define (rankReduce results next)
  (cond ((= (cadr next) 0) results)
	((null? results) (list next))
        ((> (cadr next) (cadar results)) (cons next results))
        (else (cons (car results) (rankReduce (cdr results) next)))))

;definition using mapreduce :D
(define ranking
  (lambda (word pages)
    (mapreduce (rankMap word pages) rankReduce '() pages)))

;;;;;;;;;;
;;try it;;
;;;;;;;;;;
(ranking theWord theList)

;Finally gSearch is just this
(define (gSearch word pages) (map car (ranking word pages)))

;the is not a relevant word, so it returns empty list
(gSearch theWord theList)

(gSearch 'Lorem loremipsum)
(tf-idf 'Lorem loremipsum)
