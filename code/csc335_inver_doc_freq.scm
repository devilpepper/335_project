(define find-words
  (lambda(word-list)
    (if (null? word-list) '()
        (find-words-aux (car word-list) (cdr word-list) '() ))))


(define find-words-aux
  (lambda(first-element remaining-list tmp )
    (if(null? remaining-list) (cons (cons 1 first-element) tmp)
        (find-words-aux (car remaining-list)  (cdr remaining-list) (cons (cons 1 first-element) tmp)))))

  


(define test-text ( list (list 'the 'cat 'in 'the 'hat) (list 'in 'no 'nonsense 'fashion 'in 'no)
                         (list 'the 'in 'a 'the 'ant 'cant 'cant 'in 'the)))

;do something like this to find frequency
; (if(equal? first-element (car remaining-list)) (freq-aux first-element (+ 1 n) (cdr remaining-list) tmp)
;(define reduce
;  (lambda(op list)))
    

(define t (list 'the 'cat 'in 'the 'hat))

(map find-words test-text)